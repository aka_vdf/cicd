const express = require('express')
const app = express()
const version = process.env.VERSION || 'local'


const getMessage = (content) => {
  return `Hello World! \n Version: ${content}`
}

app.get('/', (req, res) => {
  res.send(getMessage(req.query.content));
})

app.get('/test/:var', (req, res) => {
  res.send('Test variable is ${req.params.var}')
})

module.exports = app;
